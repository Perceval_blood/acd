package kz.aitu.week;

import java.util.Scanner;

public class Task57 {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        boolean dateiscorrect = false;
        while (!dateiscorrect) {
            System.out.print("day");
            int day = sc.nextInt();
            System.out.print("month");
            int mon = sc.nextInt();
            System.out.print("year");
            int year = sc.nextInt();

            boolean isLeapYear = ((year % 4 == 0) && (year % 100 != 0) || (year % 400 == 0));

            int maxdays[] = new int[]{31, 28, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31};

            if (isLeapYear) {
                maxdays[1] = 29;
            }
            boolean monisok = ((mon > 0) & (mon < 13));

            if (monisok) {
                dateiscorrect = (day <= maxdays[mon - 1]);
            }
            if (!dateiscorrect) {
                System.out.println("INCORRECT");
            } else {
                System.out.println("CORRECT");
            }
        }
    }
}

