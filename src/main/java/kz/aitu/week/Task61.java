package kz.aitu.week;
import java.util.Scanner;
import static java.lang.Math.pow;
import static java.lang.Math.sqrt;
public class Task61 {
    public static void main(String[] args) {
        Scanner scanner=new Scanner(System.in);
        int a=scanner.nextInt();
        int b=scanner.nextInt();
        int c=scanner.nextInt();
        double x,x1,x2,d;
        d=pow(b,2)-4*a*c;
        if(d>0){
            x1=(-b+sqrt(d))/2*a;
            x2=(-b-sqrt(d))/2*a;
            System.out.println(x1+" "+x2);
        }
        else if (d==0){
            x=-b/2*a;
            System.out.println(x);
        }
             else  {
            System.out.println("error");
        }
    }
}
