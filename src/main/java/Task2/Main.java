package Task2;

public class Main {
    public static void main(String[] args) {
        Person person_1 = new Person();
        Person person_2 = new Person();
        Person person_3 = new Person();
        Person person_4 = new Person();
        Person person_5 = new Person();

        person_1.input("Bekasu", 2000);
        person_2.input("Temir", 1999);
        person_3.input("Sharitarizawa", 1998);
        person_4.input("Diana", 1997);
        person_5.input("Bakyt", 1996);

        System.out.println("Person 1  " + person_1.getName() + " " + person_1.getBirthYear());
        System.out.println("Person 2  " + person_2.getName() + " " + person_2.getBirthYear());
        System.out.println("Person 3  " + person_3.getName() + " " + person_3.getBirthYear());
        System.out.println("Person 4  " + person_4.getName() + " " + person_4.getBirthYear());
        System.out.println("Person 5  " + person_5.getName() + " " + person_5.getBirthYear());

        person_1.age();
        person_2.age();
        person_3.age();
        person_4.age();
        person_5.age();

        person_1.changeName("Aya");
        person_2.changeName("Asyltas");
        person_3.changeName("Aikyn");
        person_4.changeName("Bakytzhan");
        person_5.changeName("Dauren");

        System.out.println("Person 1 : " + person_1.getName() + " " + person_1.getBirthYear());
        System.out.println("Person 2 : " + person_2.getName() + " " + person_2.getBirthYear());
        System.out.println("Person 3 : " + person_3.getName() + " " + person_3.getBirthYear());
        System.out.println("Person 4 : " + person_4.getName() + " " + person_4.getBirthYear());
        System.out.println("Person 5 : " + person_5.getName() + " " + person_5.getBirthYear());
    }

}
