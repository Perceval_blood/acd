package OOP;

public class Main {
    public static void main(String[] args) {
        Employee employee_1 = new Employee();
        Employee employee_2 = new Employee();
        Employee employee_3 = new Employee();

        employee_1.setRate(44);
        employee_1.setHours(17);
        employee_1.setName("Indira");

        employee_2.setRate(28);
        employee_2.setHours(14);
        employee_2.setName("Era");

        employee_3.setRate(23);
        employee_3.setHours(39);
        employee_3.setName("Ali");

        employee_1.salary("Indira");
        employee_1.toString("Indira");
        employee_1.changeRate("Indira", 20);
        employee_1.bonuses("Indira");

        System.out.println("1st employee's name is " + employee_1.getName());
        employee_1.salary();
        employee_1.toString();
        employee_1.changeRate(50);
        employee_1.bonuses();

        System.out.println("2nd employee's name is " + employee_2.getName());
        employee_2.salary();
        employee_2.toString();
        employee_2.changeRate(47);
        employee_2.bonuses();

        System.out.println("3rd employee's name is ");
        employee_3.salary();
        employee_3.toString();
        employee_3.changeRate(88);
        employee_3.bonuses();

        System.out.println("Hours of all employees");
        System.out.println("Hours of 1st employee is");
        System.out.println(employee_1.getName() + "  " + employee_1.getHours());

        System.out.println("Hours of 2nd employee is");
        System.out.println(employee_2.getName() + "  " + employee_2.getHours());

        System.out.println("Hours of 3rd employee is");
        System.out.println(employee_3.getName() + "  " + employee_3.getHours());
    }

}
